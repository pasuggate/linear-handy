{-# LANGUAGE DeriveDataTypeable, ScopedTypeVariables, FlexibleInstances, CPP,
             RankNTypes, TypeFamilies, MultiParamTypeClasses, DeriveGeneric,
             Trustworthy
  #-}
------------------------------------------------------------------------------
-- |
-- Module      : Linear.V6
-- Copyright   : (C) 2012-2015 Edward Kmett
--             : (C) Patrick Suggate, 2019
-- License     : BSD-style (see the file LICENSE)
-- 
-- Maintainer  : Patrick Suggate
-- Stability   : experimental
-- Portability : non-portable
-- 
-- 6D vectors.
-- 
------------------------------------------------------------------------------

module Linear.V6
  ( V5 (..)
  , V6 (..)
  , R1 (..)
  , R2 (..)
  , _yx
  , R3 (..)
  , _xz, _yz, _zx, _zy
  , _xzy, _yxz, _yzx, _zxy, _zyx
  , R4 (..)
  , R5 (..)
  , R6 (..)
  , _xw, _yw, _zw, _wx, _wy, _wz
  , _xyw, _xzw, _xwy, _xwz, _yxw, _yzw, _ywx, _ywz, _zxw, _zyw, _zwx, _zwy
  , _wxy, _wxz, _wyx, _wyz, _wzx, _wzy
  , _xywz, _xzyw, _xzwy, _xwyz, _xwzy, _yxzw , _yxwz, _yzxw, _yzwx, _ywxz
  , _ywzx, _zxyw, _zxwy, _zyxw, _zywx, _zwxy, _zwyx, _wxyz, _wxzy, _wyxz
  , _wyzx, _wzxy, _wzyx
  , eu, ev, ew, ex, ey, ez
  ) where

import Control.Applicative
import Control.DeepSeq (NFData(rnf))
import Control.Monad (liftM)
import Control.Monad.Fix
import Control.Monad.Zip
import Control.Lens hiding ((<.>))
import Data.Binary as Binary
import Data.Bytes.Serial
import Data.Data
import Data.Distributive
import Data.Foldable
import Data.Functor.Bind
import Data.Functor.Classes
import Data.Functor.Rep
import Data.Hashable
import Data.Semigroup
import Data.Semigroup.Foldable
import Data.Serialize as Cereal
import Foreign.Ptr (castPtr)
import Foreign.Storable (Storable(..))
import GHC.Arr (Ix(..))
import GHC.Generics (Generic, Generic1)
import qualified Data.Vector.Generic.Mutable as M
import qualified Data.Vector.Generic as G
import qualified Data.Vector.Unboxed.Base as U
import Linear.Epsilon
import Linear.Metric
import Linear.V2
import Linear.V3
import Linear.V4
import Linear.Vector

{-# ANN module "HLint: ignore Reduce duplication" #-}

------------------------------------------------------------------------------
-- | A 5-dimensional vector.
--   
--   TODO: how complete is V5 support?
data V5 a =
  V5 !a !a !a !a !a
  deriving (Eq, Ord, Show, Read, Data, Typeable, Generic, Generic1)

instance Functor V5 where
  {-# INLINE fmap #-}
  {-# INLINE (<$) #-}
  fmap f (V5 v w x y z) = V5 (f v) (f w) (f x) (f y) (f z)
  a <$ _ = V5 a a a a a

instance Foldable V5 where
  {-# INLINE foldMap #-}
  foldMap f (V5 v w x y z) =
    f v `mappend` f w `mappend` f x `mappend` f y `mappend` f z

instance Traversable V5 where
  {-# INLINE traverse #-}
  traverse f (V5 v w x y z) = V5 <$> f v <*> f w <*> f x <*> f y <*> f z

------------------------------------------------------------------------------
-- | A 6-dimensional vector.
data V6 a =
  V6 !a !a !a !a !a !a
  deriving (Eq, Ord, Show, Read, Data, Typeable, Generic, Generic1)

instance Functor V6 where
  {-# INLINE fmap #-}
  {-# INLINE (<$) #-}
  fmap f (V6 u v w x y z) = V6 (f u) (f v) (f w) (f x) (f y) (f z)
  a <$ _ = V6 a a a a a a

instance Foldable V6 where
  {-# INLINE foldMap #-}
  foldMap f (V6 u v w x y z) =
    f u `mappend` f v `mappend` f w `mappend` f x `mappend` f y `mappend` f z

instance Traversable V6 where
  {-# INLINE traverse #-}
  traverse f (V6 u v w x y z) =
    V6 <$> f u <*> f v <*> f w <*> f x <*> f y <*> f z

instance Foldable1 V6 where
  {-# INLINE foldMap1 #-}
  foldMap1 f (V6 u v w x y z) = f u <> f v <> f w <> f x <> f y <> f z

instance Traversable1 V6 where
  traverse1 f (V6 u v w x y z) =
    V6 <$> f u <.> f v <.> f w <.> f x <.> f y <.> f z
  {-# INLINE traverse1 #-}

instance Applicative V6 where
  {-# INLINE pure  #-}
  {-# INLINE (<*>) #-}
  pure a = V6 a a a a a a
  V6 u v w x y z <*> V6 a b c d e f = V6 (u a) (v b) (w c) (x d) (y e) (z f)

instance Apply V6 where
  {-# INLINE (<.>) #-}
  V6 u v w x y z <.> V6 a b c d e f = V6 (u a) (v b) (w c) (x d) (y e) (z f)

instance Additive V6 where
  zero = pure 0
  {-# INLINE zero #-}
  liftU2 = liftA2
  {-# INLINE liftU2 #-}
  liftI2 = liftA2
  {-# INLINE liftI2 #-}

instance Bind V6 where
  {-# INLINE (>>-) #-}
  V6 u v w x y z >>- f = V6 u' v' w' x' y' z' where
    V6 u' _ _ _ _ _ = f u
    V6 _ v' _ _ _ _ = f v
    V6 _ _ w' _ _ _ = f w
    V6 _ _ _ x' _ _ = f x
    V6 _ _ _ _ y' _ = f y
    V6 _ _ _ _ _ z' = f z

instance Monad V6 where
  {-# INLINE return #-}
  {-# INLINE (>>=)  #-}
  return a = V6 a a a a a a
  V6 u v w x y z >>= f = V6 u' v' w' x' y' z' where
    V6 u' _ _ _ _ _ = f u
    V6 _ v' _ _ _ _ = f v
    V6 _ _ w' _ _ _ = f w
    V6 _ _ _ x' _ _ = f x
    V6 _ _ _ _ y' _ = f y
    V6 _ _ _ _ _ z' = f z

instance Num a => Num (V6 a) where
  (+) = liftA2 (+)
  {-# INLINE (+) #-}
  (*) = liftA2 (*)
  {-# INLINE (-) #-}
  (-) = liftA2 (-)
  {-# INLINE (*) #-}
  negate = fmap negate
  {-# INLINE negate #-}
  abs = fmap abs
  {-# INLINE abs #-}
  signum = fmap signum
  {-# INLINE signum #-}
  fromInteger = pure . fromInteger
  {-# INLINE fromInteger #-}

instance Fractional a => Fractional (V6 a) where
  recip = fmap recip
  {-# INLINE recip #-}
  (/) = liftA2 (/)
  {-# INLINE (/) #-}
  fromRational = pure . fromRational
  {-# INLINE fromRational #-}

instance Floating a => Floating (V6 a) where
    pi = pure pi
    {-# INLINE pi #-}
    exp = fmap exp
    {-# INLINE exp #-}
    sqrt = fmap sqrt
    {-# INLINE sqrt #-}
    log = fmap log
    {-# INLINE log #-}
    (**) = liftA2 (**)
    {-# INLINE (**) #-}
    logBase = liftA2 logBase
    {-# INLINE logBase #-}
    sin = fmap sin
    {-# INLINE sin #-}
    tan = fmap tan
    {-# INLINE tan #-}
    cos = fmap cos
    {-# INLINE cos #-}
    asin = fmap asin
    {-# INLINE asin #-}
    atan = fmap atan
    {-# INLINE atan #-}
    acos = fmap acos
    {-# INLINE acos #-}
    sinh = fmap sinh
    {-# INLINE sinh #-}
    tanh = fmap tanh
    {-# INLINE tanh #-}
    cosh = fmap cosh
    {-# INLINE cosh #-}
    asinh = fmap asinh
    {-# INLINE asinh #-}
    atanh = fmap atanh
    {-# INLINE atanh #-}
    acosh = fmap acosh
    {-# INLINE acosh #-}

instance Metric V6 where
  {-# INLINE dot #-}
  dot (V6 u v w x y z) (V6 a b c d e f) = u*a + v*b + w*c + x*d + y*e + z*f

instance Distributive V6 where
  distribute f = V6 (fmap (\(V6 u _ _ _ _ _) -> u) f)
                    (fmap (\(V6 _ v _ _ _ _) -> v) f)
                    (fmap (\(V6 _ _ w _ _ _) -> w) f)
                    (fmap (\(V6 _ _ _ x _ _) -> x) f)
                    (fmap (\(V6 _ _ _ _ y _) -> y) f)
                    (fmap (\(V6 _ _ _ _ _ z) -> z) f)
  {-# INLINE distribute #-}

instance Hashable a => Hashable (V6 a) where
  {-# INLINE hashWithSalt #-}
  hashWithSalt s (V6 u v w x y z) =
    s `hashWithSalt` u `hashWithSalt` v `hashWithSalt`
    w `hashWithSalt` x `hashWithSalt` y `hashWithSalt` z

{-- }
_xw, _yw, _zw, _wx, _wy, _wz :: R4 t => Lens' (t a) (V2 a)
_xw f = _xyzw $ \(V6 u v w x y z) -> f (V2 a d) <&> \(V2 a' d') -> V6 a' b c d'
{-# INLINE _xw #-}

_yw f = _xyzw $ \(V6 u v w x y z) -> f (V2 b d) <&> \(V2 b' d') -> V6 a b' c d'
{-# INLINE _yw #-}

_zw f = _xyzw $ \(V6 u v w x y z) -> f (V2 c d) <&> \(V2 c' d') -> V6 a b c' d'
{-# INLINE _zw #-}

_wx f = _xyzw $ \(V6 u v w x y z) -> f (V2 d a) <&> \(V2 d' a') -> V6 a' b c d'
{-# INLINE _wx #-}

_wy f = _xyzw $ \(V6 u v w x y z) -> f (V2 d b) <&> \(V2 d' b') -> V6 a b' c d'
{-# INLINE _wy #-}

_wz f = _xyzw $ \(V6 u v w x y z) -> f (V2 d c) <&> \(V2 d' c') -> V6 a b c' d'
{-# INLINE _wz #-}

_xyw, _xzw, _xwy, _xwz, _yxw, _yzw, _ywx, _ywz, _zxw, _zyw, _zwx, _zwy, _wxy, _wxz, _wyx, _wyz, _wzx, _wzy :: R4 t => Lens' (t a) (V3 a)
_xyw f = _xyzw $ \(V6 u v w x y z) -> f (V3 a b d) <&> \(V3 a' b' d') -> V6 a' b' c d'
{-# INLINE _xyw #-}

_xzw f = _xyzw $ \(V6 u v w x y z) -> f (V3 a c d) <&> \(V3 a' c' d') -> V6 a' b c' d'
{-# INLINE _xzw #-}

_xwy f = _xyzw $ \(V6 u v w x y z) -> f (V3 a d b) <&> \(V3 a' d' b') -> V6 a' b' c d'
{-# INLINE _xwy #-}

_xwz f = _xyzw $ \(V6 u v w x y z) -> f (V3 a d c) <&> \(V3 a' d' c') -> V6 a' b c' d'
{-# INLINE _xwz #-}

_yxw f = _xyzw $ \(V6 u v w x y z) -> f (V3 b a d) <&> \(V3 b' a' d') -> V6 a' b' c d'
{-# INLINE _yxw #-}

_yzw f = _xyzw $ \(V6 u v w x y z) -> f (V3 b c d) <&> \(V3 b' c' d') -> V6 a b' c' d'
{-# INLINE _yzw #-}

_ywx f = _xyzw $ \(V6 u v w x y z) -> f (V3 b d a) <&> \(V3 b' d' a') -> V6 a' b' c d'
{-# INLINE _ywx #-}

_ywz f = _xyzw $ \(V6 u v w x y z) -> f (V3 b d c) <&> \(V3 b' d' c') -> V6 a b' c' d'
{-# INLINE _ywz #-}

_zxw f = _xyzw $ \(V6 u v w x y z) -> f (V3 c a d) <&> \(V3 c' a' d') -> V6 a' b c' d'
{-# INLINE _zxw #-}

_zyw f = _xyzw $ \(V6 u v w x y z) -> f (V3 c b d) <&> \(V3 c' b' d') -> V6 a b' c' d'
{-# INLINE _zyw #-}

_zwx f = _xyzw $ \(V6 u v w x y z) -> f (V3 c d a) <&> \(V3 c' d' a') -> V6 a' b c' d'
{-# INLINE _zwx #-}

_zwy f = _xyzw $ \(V6 u v w x y z) -> f (V3 c d b) <&> \(V3 c' d' b') -> V6 a b' c' d'
{-# INLINE _zwy #-}

_wxy f = _xyzw $ \(V6 u v w x y z) -> f (V3 d a b) <&> \(V3 d' a' b') -> V6 a' b' c d'
{-# INLINE _wxy #-}

_wxz f = _xyzw $ \(V6 u v w x y z) -> f (V3 d a c) <&> \(V3 d' a' c') -> V6 a' b c' d'
{-# INLINE _wxz #-}

_wyx f = _xyzw $ \(V6 u v w x y z) -> f (V3 d b a) <&> \(V3 d' b' a') -> V6 a' b' c d'
{-# INLINE _wyx #-}

_wyz f = _xyzw $ \(V6 u v w x y z) -> f (V3 d b c) <&> \(V3 d' b' c') -> V6 a b' c' d'
{-# INLINE _wyz #-}

_wzx f = _xyzw $ \(V6 u v w x y z) -> f (V3 d c a) <&> \(V3 d' c' a') -> V6 a' b c' d'
{-# INLINE _wzx #-}

_wzy f = _xyzw $ \(V6 u v w x y z) -> f (V3 d c b) <&> \(V3 d' c' b') -> V6 a b' c' d'
{-# INLINE _wzy #-}

_xywz, _xzyw, _xzwy, _xwyz, _xwzy, _yxzw , _yxwz, _yzxw, _yzwx, _ywxz
  , _ywzx, _zxyw, _zxwy, _zyxw, _zywx, _zwxy, _zwyx, _wxyz, _wxzy, _wyxz
  , _wyzx, _wzxy, _wzyx :: R4 t => Lens' (t a) (V6 a)
_xywz f = _xyzw $ \(V6 u v w x y z) -> f (V6 a b d c) <&> \(V6 a' b' d' c') -> V6 a' b' c' d'
{-# INLINE _xywz #-}

_xzyw f = _xyzw $ \(V6 u v w x y z) -> f (V6 a c b d) <&> \(V6 a' c' b' d') -> V6 a' b' c' d'
{-# INLINE _xzyw #-}

_xzwy f = _xyzw $ \(V6 u v w x y z) -> f (V6 a c d b) <&> \(V6 a' c' d' b') -> V6 a' b' c' d'
{-# INLINE _xzwy #-}

_xwyz f = _xyzw $ \(V6 u v w x y z) -> f (V6 a d b c) <&> \(V6 a' d' b' c') -> V6 a' b' c' d'
{-# INLINE _xwyz #-}

_xwzy f = _xyzw $ \(V6 u v w x y z) -> f (V6 a d c b) <&> \(V6 a' d' c' b') -> V6 a' b' c' d'
{-# INLINE _xwzy #-}

_yxzw f = _xyzw $ \(V6 u v w x y z) -> f (V6 b a c d) <&> \(V6 b' a' c' d') -> V6 a' b' c' d'
{-# INLINE _yxzw #-}

_yxwz f = _xyzw $ \(V6 u v w x y z) -> f (V6 b a d c) <&> \(V6 b' a' d' c') -> V6 a' b' c' d'
{-# INLINE _yxwz #-}

_yzxw f = _xyzw $ \(V6 u v w x y z) -> f (V6 b c a d) <&> \(V6 b' c' a' d') -> V6 a' b' c' d'
{-# INLINE _yzxw #-}

_yzwx f = _xyzw $ \(V6 u v w x y z) -> f (V6 b c d a) <&> \(V6 b' c' d' a') -> V6 a' b' c' d'
{-# INLINE _yzwx #-}

_ywxz f = _xyzw $ \(V6 u v w x y z) -> f (V6 b d a c) <&> \(V6 b' d' a' c') -> V6 a' b' c' d'
{-# INLINE _ywxz #-}

_ywzx f = _xyzw $ \(V6 u v w x y z) -> f (V6 b d c a) <&> \(V6 b' d' c' a') -> V6 a' b' c' d'
{-# INLINE _ywzx #-}

_zxyw f = _xyzw $ \(V6 u v w x y z) -> f (V6 c a b d) <&> \(V6 c' a' b' d') -> V6 a' b' c' d'
{-# INLINE _zxyw #-}

_zxwy f = _xyzw $ \(V6 u v w x y z) -> f (V6 c a d b) <&> \(V6 c' a' d' b') -> V6 a' b' c' d'
{-# INLINE _zxwy #-}

_zyxw f = _xyzw $ \(V6 u v w x y z) -> f (V6 c b a d) <&> \(V6 c' b' a' d') -> V6 a' b' c' d'
{-# INLINE _zyxw #-}

_zywx f = _xyzw $ \(V6 u v w x y z) -> f (V6 c b d a) <&> \(V6 c' b' d' a') -> V6 a' b' c' d'
{-# INLINE _zywx #-}

_zwxy f = _xyzw $ \(V6 u v w x y z) -> f (V6 c d a b) <&> \(V6 c' d' a' b') -> V6 a' b' c' d'
{-# INLINE _zwxy #-}

_zwyx f = _xyzw $ \(V6 u v w x y z) -> f (V6 c d b a) <&> \(V6 c' d' b' a') -> V6 a' b' c' d'
{-# INLINE _zwyx #-}

_wxyz f = _xyzw $ \(V6 u v w x y z) -> f (V6 d a b c) <&> \(V6 d' a' b' c') -> V6 a' b' c' d'
{-# INLINE _wxyz #-}

_wxzy f = _xyzw $ \(V6 u v w x y z) -> f (V6 d a c b) <&> \(V6 d' a' c' b') -> V6 a' b' c' d'
{-# INLINE _wxzy #-}

_wyxz f = _xyzw $ \(V6 u v w x y z) -> f (V6 d b a c) <&> \(V6 d' b' a' c') -> V6 a' b' c' d'
{-# INLINE _wyxz #-}

_wyzx f = _xyzw $ \(V6 u v w x y z) -> f (V6 d b c a) <&> \(V6 d' b' c' a') -> V6 a' b' c' d'
{-# INLINE _wyzx #-}

_wzxy f = _xyzw $ \(V6 u v w x y z) -> f (V6 d c a b) <&> \(V6 d' c' a' b') -> V6 a' b' c' d'
{-# INLINE _wzxy #-}

_wzyx f = _xyzw $ \(V6 u v w x y z) -> f (V6 d c b a) <&> \(V6 d' c' b' a') -> V6 a' b' c' d'
{-# INLINE _wzyx #-}
--}

------------------------------------------------------------------------------
-- | A space that distinguishes orthogonal basis vectors '_u', '_v', '_w',
--   '_x', '_y', '_z'. (It may have more.)
class R4 t => R5 t where
  -- |
  -- >>> V5 1 2 3 4 5 ^._v
  -- 1
  _v :: Lens' (t a) a
  _vwxyz :: Lens' (t a) (V5 a)

class R5 t => R6 t where
  -- |
  -- >>> V6 1 2 3 4 5 6 ^._u
  -- 1
  _u :: Lens' (t a) a
  _uvwxyz :: Lens' (t a) (V6 a)

eu :: R6 t => E t
eu = E _u

ev :: R5 t => E t
ev = E _v

instance R1 V6 where
  _x f (V6 u v w x y z) = (\x' -> V6 u v w x' y z) <$> f x
  {-# INLINE _x #-}

instance R2 V6 where
  _y f (V6 u v w x y z) = (\y' -> V6 u v w x y' z) <$> f y
  {-# INLINE _y #-}
  _xy f (V6 u v w x y z) = (\(V2 x' y') -> V6 u v w x' y' z) <$> f (V2 x y)
  {-# INLINE _xy #-}

instance R3 V6 where
  _z f (V6 u v w x y z) = (\x' -> V6 u v w x' y z) <$> f x
  {-# INLINE _z #-}
  _xyz f (V6 u v w x y z) = (\(V3 x' y' z') -> V6 u v w x' y' z') <$> f (V3 x y z)
  {-# INLINE _xyz #-}

instance R4 V6 where
  {-# INLINE _w #-}
  {-# INLINE _xyzw #-}
  _w f (V6 u v w x y z) = (\w' -> V6 u v w' x y z) <$> f w
  _xyzw f (V6 u v w x y z) =
    (\(V4 w' x' y' z') -> V6 u v w' x' y' z') <$> f (V4 w x y z)

instance R5 V6 where
  {-# INLINE _v #-}
  {-# INLINE _vwxyz #-}
  _v f (V6 u v w x y z) = (\v' -> V6 u v' w x y z) <$> f v
  _vwxyz f (V6 u v w x y z) =
    (\(V5 v' w' x' y' z') -> V6 u v' w' x' y' z') <$> f (V5 v w x y z)

instance R6 V6 where
  {-# INLINE _u #-}
  {-# INLINE _uvwxyz #-}
  _u f (V6 u v w x y z) = (\u' -> V6 u' v w x y z) <$> f u
  _uvwxyz = id

instance Storable a => Storable (V6 a) where
  sizeOf _ = 6 * sizeOf (undefined::a)
  {-# INLINE sizeOf #-}
  alignment _ = alignment (undefined::a)
  {-# INLINE alignment #-}
  poke ptr (V6 u v w x y z) = do poke ptr' u
                                 pokeElemOff ptr' 1 v
                                 pokeElemOff ptr' 2 w
                                 pokeElemOff ptr' 3 x
                                 pokeElemOff ptr' 4 y
                                 pokeElemOff ptr' 5 z
    where ptr' = castPtr ptr
  {-# INLINE poke #-}
  peek ptr = V6 <$> peek ptr' <*> peekElemOff ptr' 1
                <*> peekElemOff ptr' 2 <*> peekElemOff ptr' 3
                <*> peekElemOff ptr' 4 <*> peekElemOff ptr' 5
    where ptr' = castPtr ptr
  {-# INLINE peek #-}

instance Epsilon a => Epsilon (V6 a) where
  nearZero = nearZero . quadrance
  {-# INLINE nearZero #-}

{-- }
instance Ix a => Ix (V6 a) where
  {-# SPECIALISE instance Ix (V6 Int) #-}

  range (V6 l1 l2 l3 l4,V6 u1 u2 u3 u4) =
    [V6 i1 i2 i3 i4 | i1 <- range (l1,u1)
                    , i2 <- range (l2,u2)
                    , i3 <- range (l3,u3)
                    , i4 <- range (l4,u4)
                    ]
  {-# INLINE range #-}

  unsafeIndex (V6 l1 l2 l3 l4,V6 u1 u2 u3 u4) (V6 i1 i2 i3 i4) =
    unsafeIndex (l4,u4) i4 + unsafeRangeSize (l4,u4) * (
    unsafeIndex (l3,u3) i3 + unsafeRangeSize (l3,u3) * (
    unsafeIndex (l2,u2) i2 + unsafeRangeSize (l2,u2) *
    unsafeIndex (l1,u1) i1))
  {-# INLINE unsafeIndex #-}

  inRange (V6 l1 l2 l3 l4,V6 u1 u2 u3 u4) (V6 i1 i2 i3 i4) =
    inRange (l1,u1) i1 && inRange (l2,u2) i2 &&
    inRange (l3,u3) i3 && inRange (l4,u4) i4
  {-# INLINE inRange #-}
--}

instance Representable V6 where
  {-# INLINE tabulate #-}
  {-# INLINE index    #-}
  type Rep V6 = E V6
  tabulate f = V6 (f eu) (f ev) (f ew) (f ex) (f ey) (f ez)
  index xs (E l) = view l xs

instance FunctorWithIndex (E V6) V6 where
  {-# INLINE imap #-}
  imap f (V6 u v w x y z) =
    V6 (f eu u) (f ev v) (f ew w) (f ex x) (f ey y) (f ez z)

instance FoldableWithIndex (E V6) V6 where
  {-# INLINE ifoldMap #-}
  ifoldMap f (V6 u v w x y z) =
    f eu u `mappend` f ev v `mappend` f ew w `mappend`
    f ex x `mappend` f ey y `mappend` f ez z

instance TraversableWithIndex (E V6) V6 where
  {-# INLINE itraverse #-}
  itraverse f (V6 u v w x y z) =
    V6 <$> f eu u <*> f ev v <*> f ew w <*> f ex x <*> f ey y <*> f ez z

type instance Index (V6 a) = E V6
type instance IxValue (V6 a) = a

instance Ixed (V6 a) where
  ix = el

instance Each (V6 a) (V6 b) a b where
  each = traverse

data instance U.Vector    (V6 a) =  V_V6 {-# UNPACK #-} !Int !(U.Vector    a)
data instance U.MVector s (V6 a) = MV_V6 {-# UNPACK #-} !Int !(U.MVector s a)
instance U.Unbox a => U.Unbox (V6 a)

instance U.Unbox a => M.MVector U.MVector (V6 a) where
  basicLength (MV_V6 n _) = n
  basicUnsafeSlice m n (MV_V6 _ v) = MV_V6 n (M.basicUnsafeSlice (6*m) (6*n) v)
  basicOverlaps (MV_V6 _ v) (MV_V6 _ u) = M.basicOverlaps v u
  basicUnsafeNew n = liftM (MV_V6 n) (M.basicUnsafeNew (6*n))
  basicUnsafeRead (MV_V6 _ a) i =
    do let o = 6*i
       u <- M.basicUnsafeRead a o
       v <- M.basicUnsafeRead a (o+1)
       w <- M.basicUnsafeRead a (o+2)
       x <- M.basicUnsafeRead a (o+3)
       y <- M.basicUnsafeRead a (o+4)
       z <- M.basicUnsafeRead a (o+5)
       return (V6 u v w x y z)
  basicUnsafeWrite (MV_V6 _ a) i (V6 u v w x y z) =
    do let o = 6*i
       M.basicUnsafeWrite a o     u
       M.basicUnsafeWrite a (o+1) v
       M.basicUnsafeWrite a (o+2) w
       M.basicUnsafeWrite a (o+3) x
       M.basicUnsafeWrite a (o+4) y
       M.basicUnsafeWrite a (o+5) z
  basicInitialize (MV_V6 _ v) = M.basicInitialize v

instance U.Unbox a => G.Vector U.Vector (V6 a) where
  basicUnsafeFreeze (MV_V6 n v) = liftM ( V_V6 n) (G.basicUnsafeFreeze v)
  basicUnsafeThaw   ( V_V6 n v) = liftM (MV_V6 n) (G.basicUnsafeThaw   v)
  basicLength       ( V_V6 n _) = n
  basicUnsafeSlice m n (V_V6 _ v) = V_V6 n (G.basicUnsafeSlice (6*m) (6*n) v)
  basicUnsafeIndexM (V_V6 _ a) i =
    do let o = 6*i
       u <- G.basicUnsafeIndexM a o
       v <- G.basicUnsafeIndexM a (o+1)
       w <- G.basicUnsafeIndexM a (o+2)
       x <- G.basicUnsafeIndexM a (o+3)
       y <- G.basicUnsafeIndexM a (o+4)
       z <- G.basicUnsafeIndexM a (o+5)
       return (V6 u v w x y z)

instance MonadZip V6 where
  mzipWith = liftA2

instance MonadFix V6 where
  mfix f = V6 (let V6 a _ _ _ _ _ = f a in a)
              (let V6 _ a _ _ _ _ = f a in a)
              (let V6 _ _ a _ _ _ = f a in a)
              (let V6 _ _ _ a _ _ = f a in a)
              (let V6 _ _ _ _ a _ = f a in a)
              (let V6 _ _ _ _ _ a = f a in a)

instance Bounded a => Bounded (V6 a) where
  {-# INLINE minBound #-}
  {-# INLINE maxBound #-}
  minBound = pure minBound
  maxBound = pure maxBound

instance NFData a => NFData (V6 a) where
  rnf (V6 u v w x y z) =
    rnf u `seq` rnf v `seq` rnf w `seq` rnf x `seq` rnf y `seq` rnf z

instance Serial1 V6 where
  serializeWith     = traverse_
  deserializeWith k = V6 <$> k <*> k <*> k <*> k <*> k <*> k

instance Serial a => Serial (V6 a) where
  serialize   = serializeWith serialize
  deserialize = deserializeWith deserialize

instance Binary a => Binary (V6 a) where
  put = serializeWith   Binary.put
  get = deserializeWith Binary.get

instance Serialize a => Serialize (V6 a) where
  put = serializeWith Cereal.put
  get = deserializeWith Cereal.get

{-- }
-- TODO:
instance Eq1 V4 where
  liftEq k (V4 a b c d) (V4 e f g h) = k a e && k b f && k c g && k d h
instance Ord1 V4 where
  liftCompare k (V4 a b c d) (V4 e f g h) = k a e `mappend` k b f `mappend` k c g `mappend` k d h
instance Read1 V4 where
  liftReadsPrec k _ z = readParen (z > 10) $ \r ->
     [ (V4 a b c d, r5)
     | ("V4",r1) <- lex r
     , (a,r2) <- k 11 r1
     , (b,r3) <- k 11 r2
     , (c,r4) <- k 11 r3
     , (d,r5) <- k 11 r4
     ]
instance Show1 V4 where
  liftShowsPrec f _ z (V4 a b c d) = showParen (z > 10) $
     showString "V4 " . f 11 a . showChar ' ' . f 11 b . showChar ' ' . f 11 c . showChar ' ' . f 11 d

-- OBSOLETE: from old versions of transformers:
instance Eq1   V6 where eq1 = (==)
instance Ord1  V6 where compare1 = compare
instance Show1 V6 where showsPrec1 = showsPrec
instance Read1 V6 where readsPrec1 = readsPrec
--}
