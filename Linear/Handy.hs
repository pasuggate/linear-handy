------------------------------------------------------------------------------
-- |
-- Module      : Linear.Handy
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Just re-exporting (@Linear@-stuff) for convenience.
-- 
-- Changelog:
--  + 25/05/2019  --  initial file;
-- 
------------------------------------------------------------------------------

module Linear.Handy
  ( module Linear
  , module Linear.V6
  , module Linear.Orphans
  , module Linear.Sized
  ) where

import Linear
import Linear.V6
import Linear.Orphans
import Linear.Sized
