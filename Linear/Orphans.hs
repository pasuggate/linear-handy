{-# LANGUAGE StandaloneDeriving, DeriveGeneric
  #-}
------------------------------------------------------------------------------
-- |
-- Module      : Linear.Orphans
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Giving some (@Linear@) orphans a home.
-- 
-- Changelog:
--  + 25/05/2019  --  initial file;
-- 
-- TODO:
--  + use `TemplateHaskell` instances instead?
-- 
------------------------------------------------------------------------------

module Linear.Orphans where

import GHC.Generics (Generic)
import Foreign.C.Types
import Linear
import Linear.V6
import Data.Aeson


-- * Orphan instances
------------------------------------------------------------------------------
instance ToJSON   a => ToJSON   (V1 a)
instance FromJSON a => FromJSON (V1 a)

instance ToJSON   a => ToJSON   (V2 a)
instance FromJSON a => FromJSON (V2 a)

instance ToJSON   a => ToJSON   (V3 a)
instance FromJSON a => FromJSON (V3 a)

instance ToJSON   a => ToJSON   (V4 a)
instance FromJSON a => FromJSON (V4 a)

instance ToJSON   a => ToJSON   (V6 a)
instance FromJSON a => FromJSON (V6 a)

-- ** Orphans that require 'Generic'
------------------------------------------------------------------------------
deriving instance Generic CInt
deriving instance Generic CUInt

-- ** JSON import/export instances
------------------------------------------------------------------------------
instance ToJSON   CInt
instance FromJSON CInt

instance ToJSON   CUInt
instance FromJSON CUInt
