{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies #-}
------------------------------------------------------------------------------
-- |
-- Module      : Linear.Sized
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Gaussian quadrature points & weights
-- 
-- Changelog:
--  + 25/11/2018  --  initial file;
--  + 15/06/2019  --  moved to the 'linear-handy' repository;
-- 
------------------------------------------------------------------------------

module Linear.Sized where

import GHC.TypeNats
import Linear
import Linear.V6


-- * Sized vector type-functions
------------------------------------------------------------------------------
-- | Fixed-sized vector type-functions
type family   Vec (d :: k) a :: *

-- * Instances for sized vector type-functions
------------------------------------------------------------------------------
-- | Specialise for @Double@'s.
type instance Vec 1 a = V1 a
type instance Vec 2 a = V2 a
type instance Vec 3 a = V3 a
type instance Vec 4 a = V4 a
type instance Vec 5 a = V5 a
type instance Vec 6 a = V6 a

{-- }
-- | Specialise for @Double@'s.
type instance Vec 1 Double = V1 Double
type instance Vec 2 Double = V2 Double
type instance Vec 3 Double = V3 Double
type instance Vec 4 Double = V4 Double
type instance Vec 5 Double = V5 Double
type instance Vec 6 Double = V6 Double

-- | Specialise for @Int@'s.
type instance Vec 1 Int = V1 Int
type instance Vec 2 Int = V2 Int
type instance Vec 3 Int = V3 Int
type instance Vec 4 Int = V4 Int
type instance Vec 5 Int = V5 Int
type instance Vec 6 Int = V6 Int

-- | Specialise for @Bool@'s.
type instance Vec 1 Bool = V1 Bool
type instance Vec 2 Bool = V2 Bool
type instance Vec 3 Bool = V3 Bool
type instance Vec 4 Bool = V4 Bool
type instance Vec 5 Bool = V5 Bool
type instance Vec 6 Bool = V6 Bool
--}
